﻿using UnityEngine;

public class WheelSpinner : MonoBehaviour {
    // Speed control
    public float fastSpeed = 1.0f;
    public float mediumSpeed = 0.5f;
    public float slowSpeed = 0.01f;
    public float Speed;

    public float timer;
    public float TotalTime;

    public bool Spin_Wheel;
    public bool Start_Spin_Wheel_Check;
    public bool Check_Wheel;
    public bool Update_Location;
    public bool Start_Wheel_Spin;

    public bool Update_Ball_Location;
    public bool Updating_Ball_Location;
    public float BallDelay;

    public bool Update_Token_Location;
    public bool Updating_Token_Location;
    public float TokenDelay;

    public bool Camera_to_Table;
    public bool StartDelaying;
    public float CameraDelay;

    public Vector3 DealerTokenStart;
    public Vector3 DealerTokenHover;
    public Vector3 DealerTokenEnd;

    public Vector3 WheelCamera;
    public Vector3 WheelCameraRotation;
    public Vector3 TableCamera;
    public Vector3 TableCameraRotation;
    public Vector3 Dir_Light;
    public Vector3 Dir_Light_Rotation;

    // Use this for initialization

    public GameObject Loc_00; public GameObject Loc_01; public GameObject Loc_02;
    public GameObject Loc_03; public GameObject Loc_04; public GameObject Loc_05;
    public GameObject Loc_06; public GameObject Loc_07; public GameObject Loc_08;
    public GameObject Loc_09; public GameObject Loc_10; public GameObject Loc_11;
    public GameObject Loc_12; public GameObject Loc_13; public GameObject Loc_14;
    public GameObject Loc_15; public GameObject Loc_16; public GameObject Loc_17;
    public GameObject Loc_18; public GameObject Loc_19; public GameObject Loc_20;
    public GameObject Loc_21; public GameObject Loc_22; public GameObject Loc_23;
    public GameObject Loc_24; public GameObject Loc_25; public GameObject Loc_26;
    public GameObject Loc_27; public GameObject Loc_28; public GameObject Loc_29;
    public GameObject Loc_30; public GameObject Loc_31; public GameObject Loc_32;
    public GameObject Loc_33; public GameObject Loc_34; public GameObject Loc_35;
    public GameObject Loc_36;

    float[,] Locations = new float[37, 5];

    public GameObject Bet_00; public GameObject Bet_01; public GameObject Bet_02;
    public GameObject Bet_03; public GameObject Bet_04; public GameObject Bet_05;
    public GameObject Bet_06; public GameObject Bet_07; public GameObject Bet_08;
    public GameObject Bet_09; public GameObject Bet_10; public GameObject Bet_11;
    public GameObject Bet_12; public GameObject Bet_13; public GameObject Bet_14;
    public GameObject Bet_15; public GameObject Bet_16; public GameObject Bet_17;
    public GameObject Bet_18; public GameObject Bet_19; public GameObject Bet_20;
    public GameObject Bet_21; public GameObject Bet_22; public GameObject Bet_23;
    public GameObject Bet_24; public GameObject Bet_25; public GameObject Bet_26;
    public GameObject Bet_27; public GameObject Bet_28; public GameObject Bet_29;
    public GameObject Bet_30; public GameObject Bet_31; public GameObject Bet_32;
    public GameObject Bet_33; public GameObject Bet_34; public GameObject Bet_35;
    public GameObject Bet_36;

    public GameObject RoulletteBall;
    public GameObject DealerToken;
    public GameObject MainCamera;
    public GameObject DirectionalLight;
    
    public GameObject SpinButton;

    // reference to the Betting Tracke object
    private FieldBets fldBet;
    public Rigidbody rb;

    void Start () {

        fldBet = GameObject.Find("BettingTracker").GetComponent<FieldBets>();

	}

    void RelocateTokenToHover()
    {
        Bet_00 = GameObject.Find("Bet00"); Bet_01 = GameObject.Find("Bet01"); Bet_02 = GameObject.Find("Bet02"); Bet_03 = GameObject.Find("Bet03"); Bet_04 = GameObject.Find("Bet04");
        Bet_05 = GameObject.Find("Bet05"); Bet_06 = GameObject.Find("Bet06"); Bet_07 = GameObject.Find("Bet07"); Bet_08 = GameObject.Find("Bet08"); Bet_09 = GameObject.Find("Bet09");
        Bet_10 = GameObject.Find("Bet10"); Bet_11 = GameObject.Find("Bet11"); Bet_12 = GameObject.Find("Bet12"); Bet_13 = GameObject.Find("Bet13"); Bet_14 = GameObject.Find("Bet14");
        Bet_15 = GameObject.Find("Bet15"); Bet_16 = GameObject.Find("Bet16"); Bet_17 = GameObject.Find("Bet17"); Bet_18 = GameObject.Find("Bet18"); Bet_19 = GameObject.Find("Bet19");
        Bet_20 = GameObject.Find("Bet20"); Bet_21 = GameObject.Find("Bet21"); Bet_22 = GameObject.Find("Bet22"); Bet_23 = GameObject.Find("Bet23"); Bet_24 = GameObject.Find("Bet24");
        Bet_25 = GameObject.Find("Bet25"); Bet_26 = GameObject.Find("Bet26"); Bet_27 = GameObject.Find("Bet27"); Bet_28 = GameObject.Find("Bet28"); Bet_29 = GameObject.Find("Bet29");
        Bet_30 = GameObject.Find("Bet30"); Bet_31 = GameObject.Find("Bet31"); Bet_32 = GameObject.Find("Bet22"); Bet_33 = GameObject.Find("Bet33"); Bet_34 = GameObject.Find("Bet34");
        Bet_35 = GameObject.Find("Bet35"); Bet_36 = GameObject.Find("Bet36");

        DealerToken = GameObject.Find("Token");

        DealerTokenHover.x = Bet_20.transform.position.x;
        DealerTokenHover.y = Bet_20.transform.position.y + 1.5f;
        DealerTokenHover.z = Bet_20.transform.position.z;

        DealerToken.transform.position = Vector3.MoveTowards(DealerTokenHover, DealerTokenHover, 0);
    }

    void RelocateTokenToWinningNumber()
    {

        if (Update_Token_Location == true)
        {
            Updating_Token_Location = true;
            Update_Token_Location = false;
            TokenDelay = 3.5f;
        }

        if (Updating_Token_Location == true)
        {
            if (TokenDelay > 0)
            {
                TokenDelay -= Time.deltaTime;
            }

            if (TokenDelay <= 0)
            {
                Updating_Token_Location = false;
                Bet_00 = GameObject.Find("Bet00"); Bet_01 = GameObject.Find("Bet01"); Bet_02 = GameObject.Find("Bet02"); Bet_03 = GameObject.Find("Bet03"); Bet_04 = GameObject.Find("Bet04");
                Bet_05 = GameObject.Find("Bet05"); Bet_06 = GameObject.Find("Bet06"); Bet_07 = GameObject.Find("Bet07"); Bet_08 = GameObject.Find("Bet08"); Bet_09 = GameObject.Find("Bet09");
                Bet_10 = GameObject.Find("Bet10"); Bet_11 = GameObject.Find("Bet11"); Bet_12 = GameObject.Find("Bet12"); Bet_13 = GameObject.Find("Bet13"); Bet_14 = GameObject.Find("Bet14");
                Bet_15 = GameObject.Find("Bet15"); Bet_16 = GameObject.Find("Bet16"); Bet_17 = GameObject.Find("Bet17"); Bet_18 = GameObject.Find("Bet18"); Bet_19 = GameObject.Find("Bet19");
                Bet_20 = GameObject.Find("Bet20"); Bet_21 = GameObject.Find("Bet21"); Bet_22 = GameObject.Find("Bet22"); Bet_23 = GameObject.Find("Bet23"); Bet_24 = GameObject.Find("Bet24");
                Bet_25 = GameObject.Find("Bet25"); Bet_26 = GameObject.Find("Bet26"); Bet_27 = GameObject.Find("Bet27"); Bet_28 = GameObject.Find("Bet28"); Bet_29 = GameObject.Find("Bet29");
                Bet_30 = GameObject.Find("Bet30"); Bet_31 = GameObject.Find("Bet31"); Bet_32 = GameObject.Find("Bet32"); Bet_33 = GameObject.Find("Bet33"); Bet_34 = GameObject.Find("Bet34");
                Bet_35 = GameObject.Find("Bet35"); Bet_36 = GameObject.Find("Bet36");

                DealerToken = GameObject.Find("Token");

                if (Locations[0, 3] == 0) { DealerTokenEnd = Bet_00.transform.position; }
                if (Locations[0, 3] == 1) { DealerTokenEnd = Bet_01.transform.position; }
                if (Locations[0, 3] == 2) { DealerTokenEnd = Bet_02.transform.position; }
                if (Locations[0, 3] == 3) { DealerTokenEnd = Bet_03.transform.position; }
                if (Locations[0, 3] == 4) { DealerTokenEnd = Bet_04.transform.position; }
                if (Locations[0, 3] == 5) { DealerTokenEnd = Bet_05.transform.position; }
                if (Locations[0, 3] == 6) { DealerTokenEnd = Bet_06.transform.position; }
                if (Locations[0, 3] == 7) { DealerTokenEnd = Bet_07.transform.position; }
                if (Locations[0, 3] == 8) { DealerTokenEnd = Bet_08.transform.position; }
                if (Locations[0, 3] == 9) { DealerTokenEnd = Bet_09.transform.position; }
                if (Locations[0, 3] == 10) { DealerTokenEnd = Bet_10.transform.position; }
                if (Locations[0, 3] == 11) { DealerTokenEnd = Bet_11.transform.position; }
                if (Locations[0, 3] == 12) { DealerTokenEnd = Bet_12.transform.position; }
                if (Locations[0, 3] == 13) { DealerTokenEnd = Bet_13.transform.position; }
                if (Locations[0, 3] == 14) { DealerTokenEnd = Bet_14.transform.position; }
                if (Locations[0, 3] == 15) { DealerTokenEnd = Bet_15.transform.position; }
                if (Locations[0, 3] == 16) { DealerTokenEnd = Bet_16.transform.position; }
                if (Locations[0, 3] == 17) { DealerTokenEnd = Bet_17.transform.position; }
                if (Locations[0, 3] == 18) { DealerTokenEnd = Bet_18.transform.position; }
                if (Locations[0, 3] == 19) { DealerTokenEnd = Bet_19.transform.position; }
                if (Locations[0, 3] == 20) { DealerTokenEnd = Bet_20.transform.position; }
                if (Locations[0, 3] == 21) { DealerTokenEnd = Bet_21.transform.position; }
                if (Locations[0, 3] == 22) { DealerTokenEnd = Bet_22.transform.position; }
                if (Locations[0, 3] == 23) { DealerTokenEnd = Bet_23.transform.position; }
                if (Locations[0, 3] == 24) { DealerTokenEnd = Bet_24.transform.position; }
                if (Locations[0, 3] == 25) { DealerTokenEnd = Bet_25.transform.position; }
                if (Locations[0, 3] == 26) { DealerTokenEnd = Bet_26.transform.position; }
                if (Locations[0, 3] == 27) { DealerTokenEnd = Bet_27.transform.position; }
                if (Locations[0, 3] == 28) { DealerTokenEnd = Bet_28.transform.position; }
                if (Locations[0, 3] == 29) { DealerTokenEnd = Bet_29.transform.position; }
                if (Locations[0, 3] == 30) { DealerTokenEnd = Bet_30.transform.position; }
                if (Locations[0, 3] == 31) { DealerTokenEnd = Bet_31.transform.position; }
                if (Locations[0, 3] == 32) { DealerTokenEnd = Bet_32.transform.position; }
                if (Locations[0, 3] == 33) { DealerTokenEnd = Bet_33.transform.position; }
                if (Locations[0, 3] == 34) { DealerTokenEnd = Bet_34.transform.position; }
                if (Locations[0, 3] == 35) { DealerTokenEnd = Bet_35.transform.position; }
                if (Locations[0, 3] == 36) { DealerTokenEnd = Bet_36.transform.position; }

                // notify the BettingTracker that the sping has finished and 
                // there is a new winning number
                fldBet.SetWinningNumber( (int)Locations[0, 3]);

                float offset = 0.5F;

                DealerTokenEnd.y = (DealerTokenEnd.y + offset);


                DealerTokenStart.x = DealerToken.transform.position.x;
                DealerTokenStart.y = DealerToken.transform.position.y + 1;
                DealerTokenStart.z = DealerToken.transform.position.z;

                DealerToken.transform.position = Vector3.MoveTowards(DealerTokenEnd, DealerTokenEnd, 0);
            }
        }
    }

    void GetBallLocation()
    {
        int NumberArray = 5; int NumberSlots = 37;

        if (Update_Ball_Location == true)
        {
            Updating_Ball_Location = true;
            Update_Ball_Location = false;
            BallDelay = 3.0f;
        }

        if (Updating_Ball_Location == true)
        {
            if (BallDelay > 0)
            {
                BallDelay -= Time.deltaTime;
            }

            if (BallDelay<= 0)
            {
                Updating_Ball_Location = false;
                Loc_00 = GameObject.Find("L00"); Loc_01 = GameObject.Find("L01"); Loc_02 = GameObject.Find("L02"); Loc_03 = GameObject.Find("L03"); Loc_04 = GameObject.Find("L04");
                Loc_05 = GameObject.Find("L05"); Loc_06 = GameObject.Find("L06"); Loc_07 = GameObject.Find("L07"); Loc_08 = GameObject.Find("L08"); Loc_09 = GameObject.Find("L09");
                Loc_10 = GameObject.Find("L10"); Loc_11 = GameObject.Find("L11"); Loc_12 = GameObject.Find("L12"); Loc_13 = GameObject.Find("L13"); Loc_14 = GameObject.Find("L14");
                Loc_15 = GameObject.Find("L15"); Loc_16 = GameObject.Find("L16"); Loc_17 = GameObject.Find("L17"); Loc_18 = GameObject.Find("L18"); Loc_19 = GameObject.Find("L19");
                Loc_20 = GameObject.Find("L20"); Loc_21 = GameObject.Find("L21"); Loc_22 = GameObject.Find("L22"); Loc_23 = GameObject.Find("L23"); Loc_24 = GameObject.Find("L24");
                Loc_25 = GameObject.Find("L25"); Loc_26 = GameObject.Find("L26"); Loc_27 = GameObject.Find("L27"); Loc_28 = GameObject.Find("L28"); Loc_29 = GameObject.Find("L29");
                Loc_30 = GameObject.Find("L30"); Loc_31 = GameObject.Find("L31"); Loc_32 = GameObject.Find("L22"); Loc_33 = GameObject.Find("L33"); Loc_34 = GameObject.Find("L34");
                Loc_35 = GameObject.Find("L35"); Loc_36 = GameObject.Find("L36");

                RoulletteBall = GameObject.Find("Sphere");

                float[] Temp_Location = new float[NumberArray];
                bool Sorted;

                float[] RoulletteBallLoc = new float[NumberArray];

                RoulletteBallLoc[0] = RoulletteBall.transform.position.x; RoulletteBallLoc[1] = RoulletteBall.transform.position.y; RoulletteBallLoc[2] = RoulletteBall.transform.position.z;

                Locations[0, 0] = Loc_00.transform.position.x; Locations[0, 1] = Loc_00.transform.position.y; Locations[0, 2] = Loc_00.transform.position.z;
                Locations[1, 0] = Loc_01.transform.position.x; Locations[1, 1] = Loc_01.transform.position.y; Locations[1, 2] = Loc_01.transform.position.z;
                Locations[2, 0] = Loc_02.transform.position.x; Locations[2, 1] = Loc_02.transform.position.y; Locations[2, 2] = Loc_02.transform.position.z;
                Locations[3, 0] = Loc_03.transform.position.x; Locations[3, 1] = Loc_03.transform.position.y; Locations[3, 2] = Loc_03.transform.position.z;
                Locations[4, 0] = Loc_04.transform.position.x; Locations[4, 1] = Loc_04.transform.position.y; Locations[4, 2] = Loc_04.transform.position.z;
                Locations[5, 0] = Loc_05.transform.position.x; Locations[5, 1] = Loc_05.transform.position.y; Locations[5, 2] = Loc_05.transform.position.z;
                Locations[6, 0] = Loc_06.transform.position.x; Locations[6, 1] = Loc_06.transform.position.y; Locations[6, 2] = Loc_06.transform.position.z;
                Locations[7, 0] = Loc_07.transform.position.x; Locations[7, 1] = Loc_07.transform.position.y; Locations[7, 2] = Loc_07.transform.position.z;
                Locations[8, 0] = Loc_08.transform.position.x; Locations[8, 1] = Loc_08.transform.position.y; Locations[8, 2] = Loc_08.transform.position.z;
                Locations[9, 0] = Loc_09.transform.position.x; Locations[9, 1] = Loc_09.transform.position.y; Locations[9, 2] = Loc_09.transform.position.z;
                Locations[10, 0] = Loc_10.transform.position.x; Locations[10, 1] = Loc_10.transform.position.y; Locations[10, 2] = Loc_10.transform.position.z;
                Locations[11, 0] = Loc_11.transform.position.x; Locations[11, 1] = Loc_11.transform.position.y; Locations[11, 2] = Loc_11.transform.position.z;
                Locations[12, 0] = Loc_12.transform.position.x; Locations[12, 1] = Loc_12.transform.position.y; Locations[12, 2] = Loc_12.transform.position.z;
                Locations[13, 0] = Loc_13.transform.position.x; Locations[13, 1] = Loc_13.transform.position.y; Locations[13, 2] = Loc_13.transform.position.z;
                Locations[14, 0] = Loc_14.transform.position.x; Locations[14, 1] = Loc_14.transform.position.y; Locations[14, 2] = Loc_14.transform.position.z;
                Locations[15, 0] = Loc_15.transform.position.x; Locations[15, 1] = Loc_15.transform.position.y; Locations[15, 2] = Loc_15.transform.position.z;
                Locations[16, 0] = Loc_16.transform.position.x; Locations[16, 1] = Loc_16.transform.position.y; Locations[16, 2] = Loc_16.transform.position.z;
                Locations[17, 0] = Loc_17.transform.position.x; Locations[17, 1] = Loc_17.transform.position.y; Locations[17, 2] = Loc_17.transform.position.z;
                Locations[18, 0] = Loc_18.transform.position.x; Locations[18, 1] = Loc_18.transform.position.y; Locations[18, 2] = Loc_18.transform.position.z;
                Locations[19, 0] = Loc_19.transform.position.x; Locations[19, 1] = Loc_19.transform.position.y; Locations[19, 2] = Loc_19.transform.position.z;
                Locations[20, 0] = Loc_20.transform.position.x; Locations[20, 1] = Loc_20.transform.position.y; Locations[20, 2] = Loc_20.transform.position.z;
                Locations[21, 0] = Loc_21.transform.position.x; Locations[21, 1] = Loc_21.transform.position.y; Locations[21, 2] = Loc_21.transform.position.z;
                Locations[22, 0] = Loc_22.transform.position.x; Locations[22, 1] = Loc_22.transform.position.y; Locations[22, 2] = Loc_22.transform.position.z;
                Locations[23, 0] = Loc_23.transform.position.x; Locations[23, 1] = Loc_23.transform.position.y; Locations[23, 2] = Loc_23.transform.position.z;
                Locations[24, 0] = Loc_24.transform.position.x; Locations[24, 1] = Loc_24.transform.position.y; Locations[24, 2] = Loc_24.transform.position.z;
                Locations[25, 0] = Loc_25.transform.position.x; Locations[25, 1] = Loc_25.transform.position.y; Locations[25, 2] = Loc_25.transform.position.z;
                Locations[26, 0] = Loc_26.transform.position.x; Locations[26, 1] = Loc_26.transform.position.y; Locations[26, 2] = Loc_26.transform.position.z;
                Locations[27, 0] = Loc_27.transform.position.x; Locations[27, 1] = Loc_27.transform.position.y; Locations[27, 2] = Loc_27.transform.position.z;
                Locations[28, 0] = Loc_28.transform.position.x; Locations[28, 1] = Loc_28.transform.position.y; Locations[28, 2] = Loc_28.transform.position.z;
                Locations[29, 0] = Loc_29.transform.position.x; Locations[29, 1] = Loc_29.transform.position.y; Locations[29, 2] = Loc_29.transform.position.z;
                Locations[30, 0] = Loc_30.transform.position.x; Locations[30, 1] = Loc_30.transform.position.y; Locations[30, 2] = Loc_30.transform.position.z;
                Locations[31, 0] = Loc_31.transform.position.x; Locations[31, 1] = Loc_31.transform.position.y; Locations[31, 2] = Loc_31.transform.position.z;
                Locations[32, 0] = Loc_32.transform.position.x; Locations[32, 1] = Loc_32.transform.position.y; Locations[32, 2] = Loc_32.transform.position.z;
                Locations[33, 0] = Loc_33.transform.position.x; Locations[33, 1] = Loc_33.transform.position.y; Locations[33, 2] = Loc_33.transform.position.z;
                Locations[34, 0] = Loc_34.transform.position.x; Locations[34, 1] = Loc_34.transform.position.y; Locations[34, 2] = Loc_34.transform.position.z;
                Locations[35, 0] = Loc_35.transform.position.x; Locations[35, 1] = Loc_35.transform.position.y; Locations[35, 2] = Loc_35.transform.position.z;
                Locations[36, 0] = Loc_36.transform.position.x; Locations[36, 1] = Loc_36.transform.position.y; Locations[36, 2] = Loc_36.transform.position.z;

                for (int i = 0; i < NumberSlots; i++)
                {
                    Locations[i, 3] = i;
                    Locations[i, 4] = Mathf.Sqrt((Locations[i, 0] - RoulletteBallLoc[0]) * (Locations[i, 0] - RoulletteBallLoc[0]) + (Locations[i, 2] - RoulletteBallLoc[2]) * (Locations[i, 2] - RoulletteBallLoc[2]));
                }

                // Sort the values.
                Sorted = true;

                while (Sorted == true)
                {
                    Sorted = false;
                    for (int i = 0; i < (NumberSlots - 1); i++)
                    {
                        if (Locations[i, 4] > Locations[(i + 1), 4])
                        {
                            Sorted = true;

                            Temp_Location[0] = Locations[i, 0];
                            Temp_Location[1] = Locations[i, 1];
                            Temp_Location[2] = Locations[i, 2];
                            Temp_Location[3] = Locations[i, 3];
                            Temp_Location[4] = Locations[i, 4];

                            Locations[i, 0] = Locations[(i + 1), 0];
                            Locations[i, 1] = Locations[(i + 1), 1];
                            Locations[i, 2] = Locations[(i + 1), 2];
                            Locations[i, 3] = Locations[(i + 1), 3];
                            Locations[i, 4] = Locations[(i + 1), 4];

                            Locations[(i + 1), 0] = Temp_Location[0];
                            Locations[(i + 1), 1] = Temp_Location[1];
                            Locations[(i + 1), 2] = Temp_Location[2];
                            Locations[(i + 1), 3] = Temp_Location[3];
                            Locations[(i + 1), 4] = Temp_Location[4];
                        }
                    }
                }
            }
        }
        // Display the values.

        // This code to be used for debuging purposes
        // print("Ball X -" + RoulletteBallLoc[0] + "Ball Z -" + RoulletteBallLoc[2]);

        for (int i = 0; i < NumberSlots; i++)
        {
            // This code to be used for debuging purposes
            // print("L" + i + " - X -" + Locations[i, 0] + " Z - " + Locations[i, 2] + " P - " + Locations[i, 3] + " D - " + Locations[i, 4]);
        }

    }

    void RelocateCamera_Wheel()
    {
        WheelCamera.x = 5.5f;
        WheelCamera.y = 9.5f;
        WheelCamera.z = -4.0f;

        WheelCameraRotation.x = 45;
        WheelCameraRotation.y = 90;
        WheelCameraRotation.z = 0;

        Dir_Light.x = 2;
        Dir_Light.y = 11;
        Dir_Light.z = 0;

        Dir_Light_Rotation.x = 50;
        Dir_Light_Rotation.y = 90;
        Dir_Light_Rotation.z = 0;
        
        MainCamera = GameObject.Find("Main Camera");
        DirectionalLight = GameObject.Find("Directional Light");

        MainCamera.transform.eulerAngles = WheelCameraRotation;
        MainCamera.transform.position = Vector3.MoveTowards(WheelCamera, WheelCamera, 0);

        DirectionalLight.transform.eulerAngles = Dir_Light_Rotation;
        DirectionalLight.transform.position = Vector3.MoveTowards(Dir_Light, Dir_Light, 0);
    }

    void RelocateCamera_Table()
    {
        Vector3 CurrentLocation;
        if (Camera_to_Table == true)
        {
            CameraDelay = 2.0f;
            StartDelaying = true;
            Camera_to_Table = false;
        }

        TableCamera.x = 12.6f;
        TableCamera.y = 14.34f;
        TableCamera.z = 4.71f;

        TableCameraRotation.x = 67.96384f;
        TableCameraRotation.y = 245.6733f;
        TableCameraRotation.z = 2.101761f;


        if (StartDelaying == true)
        {
            if (CameraDelay > 0.0f)
            {
                CameraDelay -= Time.deltaTime;
            }

            if (CameraDelay <= 0.0f)
            {
                MainCamera = GameObject.Find("Main Camera");

                CurrentLocation.x = MainCamera.transform.position.x;
                CurrentLocation.y = MainCamera.transform.position.y;
                CurrentLocation.z = MainCamera.transform.position.z;

                MainCamera.transform.eulerAngles = TableCameraRotation;
                MainCamera.transform.position = Vector3.MoveTowards( TableCamera, CurrentLocation, 0);
                StartDelaying = false;
            }
        }
    }

 
    // Update is called once per frame
    void Update () {

        Check_Wheel = Input.GetKeyDown("1");

        if (Start_Spin_Wheel_Check == true)
        {
            TotalTime = 9;
            timer = TotalTime; // number of seconds
            Start_Spin_Wheel_Check = false;

            // Move Dealer Token
            RelocateTokenToHover();
            RelocateCamera_Wheel();
        }

        if (timer > 0)
        {
            Update_Location = true;
            Spin_Wheel = true;
            timer -= Time.deltaTime;
            if (timer > (TotalTime * (2 / 3)))
            {
//                Speed = fastSpeed * Time.deltaTime;
                Speed = fastSpeed;
            }
            if ((timer < (TotalTime * (2/3))) && (timer > (TotalTime * (1 / 3))))
            {
//                Speed = mediumSpeed * Time.deltaTime;
                Speed = mediumSpeed;
            }
            if ((timer < (TotalTime * (1 / 3))) && (timer > 0))
            {
//                Speed = slowSpeed * Time.deltaTime;
                Speed = slowSpeed;
            }
        }
        else {
            Spin_Wheel = false;
            if (Update_Location == true)
            {
                Update_Location = false;
                Camera_to_Table = true;
                Update_Ball_Location = true;
                Update_Token_Location = true;
            }
        }

        RelocateCamera_Table();
        RelocateTokenToWinningNumber();
        GetBallLocation();

        if (Spin_Wheel) {
            transform.Rotate(0, Speed, 0, Space.World);
        }
    }

    public void StartSpin()
    {
            Start_Spin_Wheel_Check = true;
    }

}
