﻿using UnityEngine;
using System.Collections;

public class HintStack : MonoBehaviour {

    public string objHint;       // display the hint for the current object

    private bool displayObjHint; // should the object hint be displayed

    // initialization on loading of the objects
    void Start() {
        displayObjHint = false;
    }

    // ADD DESCRIPTION HERE
    void OnGUI()
    {
        DisplayHint();
    }

    // when the mouse enters the border of the object excute this routing
    void OnMouseEnter()
    {
        displayObjHint = true;
    }

    // when the mouse exits the border of the object excute this routing
    void OnMouseExit()
    {
        displayObjHint = false;
    }

    // executing the actual displaying of the hint in a hovering box
    void DisplayHint()
    {
        // display the hint when the mouse is hovering above the object
        if(displayObjHint)
        {
            GUI.Box(new Rect(Event.current.mousePosition.x - 55, Event.current.mousePosition.y, 50, 25),objHint);
        }
    }
}
