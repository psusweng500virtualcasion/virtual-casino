﻿/**************************************************************
 * Author: Miroslav Arsov                                     *
 * date:   October, 2015                                      *
 *                                                            *
 * Description: The FieldBets class manages all the bets,     *
 * current cash and the amount won based on an information    *
 * from outside objects. Also the class maintains references  *
 * to the Control panel and the Menu panel. Since this class  *
 * maintains all the wining amounts it is also tasked of      *
 * updating the text on the display objects in both menus.    *
 **************************************************************/

using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class FieldBets : MonoBehaviour {

    public int availableCash; // currently available cash
    public int currentBet;    // bet for the current round
    public int amountWon;     // cash won during this round
    public int winingNumber;  // the number that won in the round [1 .. 36]

    private bool rolling;             // is the ball currently rolling
    private float timeSinceLastFrame; // how much time has passed since the last frame has been drawn
    private bool delayAction;         // activate display action with some delay
    private Color originalColor;      // original menu text color on start-up


    // array of object field names
    private string[] fldNames = { // 35 to 1 payout bets
                                  "Zero",
                                  "One", "Two", "Three", "Four",
                                  "Five", "Six", "Seven", "Eight",
                                  "Nine", "Ten", "Eleven", "Twelve",
                                  "Thirteen", "Fourteen", "Fifteen", "Sixteen",
                                  "Seventeen", "Eighteen", "Nineteen", "Twenty",
                                  "TwentyOne", "TwentyTwo", "TwentyThree", "TwentyFor",
                                  "TwentyFive", "TwentySix", "TwentySeven", "TwentyEight",
                                  "TwentyNine", "Thirty", "ThirtyOne", "ThirtyTwo",
                                  "ThirtyThree", "ThirtyFour", "ThirtyFive", "ThirtySix",
                                  // 2 to 1 payout bets
                                  "FirstDozen", "SecondDozen", "ThirdDozen", "twoToOne_1", "twoToOne_2", "twoToOne_3",
                                  // 1 to 1 payout bets
                                  "OneToEighteen", "Even","Red", "Black", "Odd", "EighteenToThirtySix" };

    // lookup tables for all the different outside bets that are contained of multiple numbers
    private readonly int[] firstDozen = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
    private readonly int[] secondDozen = { 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };
    private readonly int[] thirdDozen = { 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36 };
    private readonly int[] red = { 1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36 };
    private readonly int[] black = { 2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35 };
    private readonly int[] oneToHalf = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 };
    private readonly int[] halfToEnd = { 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36 };
    private readonly int[] even = { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36 };
    private readonly int[] odd = { 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35 };
    private readonly int[] twoToOne_1 = { 1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34 };
    private readonly int[] twoToOne_2 = { 2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35 };
    private readonly int[] twoToOne_3 = { 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36 };

    // the array that contains the bets for every field
    // Ex. bet[1] => 150; this means $150 on 1(straign bet)
    private int[] bet = new int[50];

    // holds the indexes of the bets that are being displayed in the Current Bet panel
    private int[] mainDisplay = new int[5];
    private int numberOfBets = 0; // number of unique bets currently in the queue

    // reference to the displayed betting fields
    private Text[] displayFields = new Text[5];
    
    // this will contain the string representation of the fields and the payout for display on the panel
    private readonly string[,] betDisplay = { {"", "35 to 1"},
                                              {"1st. 12", "2 to 1"}, {"2nd. 12", "2 to 1"}, {"3rd. 12", "2 to 1"},
                                              { "First Row", "2 to 1"}, { "Second Row", "2 to 1"}, { "Third Row", "2 to 1"},
                                              { "1 to 18", "1 to 1"}, { "Even", "1 to 1"}, { "Red", "1 to 1"}, { "Black", "1 to 1"}, { "Odd", "1 to 1"}, { "18 to 36", "1 to 1"},
                                               };

    // initialization on loading of the objects
    void Start()
    {
        // save references to the betting display fields
        displayFields[0] = GameObject.Find("Bet_1").GetComponent<Text>();
        displayFields[1] = GameObject.Find("Bet_2").GetComponent<Text>();
        displayFields[2] = GameObject.Find("Bet_3").GetComponent<Text>();
        displayFields[3] = GameObject.Find("Bet_4").GetComponent<Text>();
        displayFields[4] = GameObject.Find("Bet_5").GetComponent<Text>();

        // initialize current bet, win amount and the rest of variables
        currentBet = 0;
        amountWon = 0;
        rolling = false;
        winingNumber = -1;

        // initialize time since last frame
        timeSinceLastFrame = 0.0f;
        delayAction = false;

        // redraw the display
        originalColor = GameObject.Find("TitleText").GetComponent<Text>().color;
        GameObject.Find("TitleText").GetComponent<Text>().text = "Current Bets";
        updateDisplay();
    }

    // Update is called once per frame
    void Update()
    {
        if(delayAction && timeSinceLastFrame < 2.0)
        {
            timeSinceLastFrame += Time.deltaTime;
        }
        else
        {
            if(amountWon > 0)
            {
                GameObject.Find("TitleText").GetComponent<Text>().color = Color.green;
                GameObject.Find("TitleText").GetComponent<Text>().text = "YOU WON";
            }
            else if(winingNumber > 0)
            {
                GameObject.Find("TitleText").GetComponent<Text>().color = Color.red;
                GameObject.Find("TitleText").GetComponent<Text>().text = "YOU LOST";
            }
            timeSinceLastFrame = 0.0f;
            delayAction = false;
        }
    }

    // what is the payout ratio for a certain field
    private int payOutMultiplier(int indexField)
    {
        int multiplier = -1; //invalid index
        if (indexField >= 0 && indexField <= 36)
            multiplier = 35;
        else if (indexField > 36 && indexField <= 42)
            multiplier = 2;
        else if (indexField > 42 && indexField <= 48)
            multiplier = 1;

        return multiplier;
    }

    // is the bet placed on fieldIndex unique;
    // are we amending a bet or adding a new one
    private bool isUnique(int fieldIndex)
    {
        for(int i=0; i<mainDisplay.Length; i++)
        {
            if (mainDisplay[i] == fieldIndex)
                return false;
        }

        return true;
    }

    // update the displays on the Current bet display and all the cash amounts as necessary
    private void updateDisplay()
    {
        // update the AvailableCash and CurrentBet labels on the control panel
        GameObject.Find("CashAmmount").GetComponent<Text>().text = availableCash.ToString();
        GameObject.Find("BetAmount").GetComponent<Text>().text = currentBet.ToString();
        GameObject.Find("WonAmmount").GetComponent<Text>().text = amountWon.ToString();

        // traverse the mainDisplay array and print the string for the bets if index is not 0
        for (int i=0; i<mainDisplay.Length; i++)
        {
            if(mainDisplay[i] != 0)
            {
                displayFields[i].text = PrintBet(fldNames[mainDisplay[i]]);
            }
            else
            {
                displayFields[i].text = "";
            }
        }
    }

    // adding bet for a certain field
    public bool CreateBet(int amount, string fldName)
    {
        bool status = false; // did the function accept the bet

        // see if there was a round before (loosing or winning) and clear it
        if(winingNumber != -1)
        {
            winingNumber = -1;
            GameObject.Find("TitleText").GetComponent<Text>().color = originalColor;
            GameObject.Find("TitleText").GetComponent<Text>().text = "Current Bets";
            availableCash += amountWon;
            amountWon = 0;
            currentBet = 0;
            ClearAllBets();
        }

        //traverse the array of string and find the index of the field name when you find it
        for (int i=0; i<fldNames.Length; i++)
        {
            if (fldName == fldNames[i] )
            {
                status = true;

                if(isUnique(i))
                {
                    // if the bet is unique add it to the display and increase the number of placed bets
                    mainDisplay[numberOfBets] = i;
                    numberOfBets++;
                }
                
                //accept the bet amount for the field
                bet[i] += amount;
                availableCash -= amount; // subtract from available cash
                currentBet += amount;

                // update the bet display
                if( !canCreatBet() )
                {
                    GameObject.Find("TitleText").GetComponent<Text>().color = originalColor;
                    GameObject.Find("TitleText").GetComponent<Text>().text = "NO MORE BETS ACCEPTED";
                }
                updateDisplay();
                break;
            }
        }

        return status;
    }

    // clearing the bet for a specific field
    public bool ClearBet(string fldName)
    {
        numberOfBets--;
        return CreateBet(0, fldName);
    }

    // cancel the bets before the roll of the ball
    public void CancelBets()
    {
        availableCash += currentBet;
        currentBet = 0;

        ClearAllBets();
    }
    
    // clear all bets
    public void ClearAllBets()
    {
        for (int i = 0; i < fldNames.Length; i++)
            bet[i] = 0;

        numberOfBets = 0;

        // clear the array of displayed values
        for (int i = 0; i < mainDisplay.Length; i++)
            mainDisplay[i] = 0;

        // update the display
        GameObject.Find("TitleText").GetComponent<Text>().color = originalColor;
        GameObject.Find("TitleText").GetComponent<Text>().text = "Current Bets";
        updateDisplay();
    }

    // get the amount that we are betting for a certain field
    public int GetBetAmmount(string fldName)
    {
        //traverse the array of string and find the index of the field name when you find it
        for (int i = 0; i < fldNames.Length; i++)
        {
            if (fldName == fldNames[i])
            {
                return bet[i];
            }
        }

        return -1; // field name was not found 
    }

    // calculate payment for a certain winning field
    public int GetPayOut(string fldName)
    {
        //traverse the array of string and find the index of the field name when you find it
        for (int i = 0; i < fldNames.Length; i++)
        {
            if (fldName == fldNames[i])
            {
                return bet[i] * payOutMultiplier(i);
            }
        }

        return -1; // no field name was not found 
    }

    // calculate total payment from all the bets
    public int GetTotalPayout()
    {
        int totalPayout = 0;

        //traverse the array of string and sum all the fields
        for (int i = 0; i < fldNames.Length; i++)
        {
            totalPayout += ( bet[i]*payOutMultiplier(i) );
        }

        return totalPayout;
    }

    // generate a string that we can display on the betting window
    public string PrintBet(string fldName)
    {
        string outString = "";
        //traverse the array of string and find the index of the field name when you find it
        for (int i = 0; i < fldNames.Length; i++)
        {
            if (fldName == fldNames[i])
            {
                if( i>=0 && i<=36 )
                    outString = "Bet " + bet[i].ToString() + "$" + " on " + i.ToString() + " at " + betDisplay[0,1];
                else if( i>36 &&  i<=48 )
                    outString = "Bet " + bet[i] + "$" + " on " + betDisplay[i - 36, 0] + " at " + betDisplay[i - 36, 1];

                break;
            }
        }

        return outString;
    }

    // can I create a bet
    public bool canCreatBet(int intendedAmount = 0)
    {
        bool status = false;
        if(!rolling && numberOfBets < mainDisplay.Length && intendedAmount <= availableCash)
        {
            status = true;
        }

        return status;
    }

    // the roll has started
    // outside functions call this function to tell it that the roll is in progress and bets cannot be accepted any longer
    public void isRollinProgress(bool isRolling)
    {
        rolling = isRolling;
    }

    // a new winning number has been generated
    public void SetWinningNumber(int winNmbr)
    {
        // display the winning number on the bets display pannel instead of the regular title
        GameObject.Find("TitleText").GetComponent<Text>().color = Color.green;
        GameObject.Find("TitleText").GetComponent<Text>().text = "The wining number is: "+ winNmbr.ToString();
        // determine if there is a straight winning bet
        amountWon += bet[winNmbr] * payOutMultiplier(winNmbr); // it's going to be either 0 or non zero
        winingNumber = winNmbr;

        if(bet[45] != 0 && red.Contains<int>(winNmbr))
        {
            // determine the red field bets
            amountWon += GetPayOut("Red");
        }
        else if (bet[46] != 0 && black.Contains<int>(winNmbr))
        {
            // determine the black field bets
            amountWon += GetPayOut("Black");
        }

        if (bet[44] != 0 && even.Contains<int>(winNmbr))
        {
            // determine the even field bets
            amountWon += GetPayOut("Even");
        }
        else if (bet[47] != 0 && odd.Contains<int>(winNmbr))
        {
            // determine the odd field bets
            amountWon += GetPayOut("Odd");
        }

        if (bet[43] != 0 && oneToHalf.Contains<int>(winNmbr))
        {
            // determine the 1 to 18 field bets
            amountWon += GetPayOut("OneToEighteen");
        }
        else if (bet[48] != 0 && halfToEnd.Contains<int>(winNmbr))
        {
            // determine the 19 to 36 field bets
            amountWon += GetPayOut("EighteenToThirtySix");
        }

        if (bet[37] != 0 && firstDozen.Contains<int>(winNmbr))
        {
            // determine the 1 to 12 field bets
            amountWon += GetPayOut("FirstDozen");
        }
        else if (bet[38] != 0 && secondDozen.Contains<int>(winNmbr))
        {
            // determine the 13 to 24 field bets
            amountWon += GetPayOut("SecondDozen");
        }
        else if (bet[39] != 0 && thirdDozen.Contains<int>(winNmbr))
        {
            // determine the 25 to 36 field bets
            amountWon += GetPayOut("ThirdDozen");
        }

        if (bet[40] != 0 && twoToOne_1.Contains<int>(winNmbr))
        {
            // determine the first row field bets
            amountWon += GetPayOut("twoToOne_1");
        }
        else if (bet[41] != 0 && twoToOne_2.Contains<int>(winNmbr))
        {
            // determine the odd field bets
            amountWon += GetPayOut("twoToOne_2");
        }
        else if (bet[42] != 0 && twoToOne_3.Contains<int>(winNmbr))
        {
            // determine the odd field bets
            amountWon += GetPayOut("twoToOne_3");
        }

        // call udpate with a delay
        delayAction = true;
        updateDisplay();
    }
}
