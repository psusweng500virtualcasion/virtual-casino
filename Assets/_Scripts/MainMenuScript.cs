﻿using UnityEngine;
using System.Collections;

public class MainMenuScript : MonoBehaviour {

    private bool _isFirstMenu = true;
    private bool _isLogInMenu = false;
    private bool _isLoadGameMenu = false;
    private bool _isLevelSelecMenu = false;
    private bool _isOptionsMenu = false;
	
    void OnGUI()
    {
        
        FirstMenu();
        LogInMenu();
        LoadGameMenu();
        LevelSelectMenu();

        if(_isLevelSelecMenu || _isLoadGameMenu || _isOptionsMenu )
        {
            if(GUI.Button(new Rect(10, Screen.height - 45, 150, 25), "Back"))
            {
                _isOptionsMenu = _isLoadGameMenu = _isLevelSelecMenu = false;
                _isLogInMenu = true;
            }
        }
    }

    void FirstMenu()
    {
        if(_isFirstMenu)
        {
            if(GUI.Button(new Rect(10, Screen.height / 2 - 100, 150, 25), "Log In"))
            {
                // we should open up a log-in window that we use to enter user/pass
                _isLogInMenu = true;
                _isFirstMenu = false;
            }
            if (GUI.Button(new Rect(10, Screen.height / 2 - 65, 150, 25), "Quit"))
            {
                Application.Quit();
            }
        }
    }

    void LoadGameMenu()
    {

    }

    void LevelSelectMenu()
    {

    }

    //this is where all the Log in menu logic should go
    void LogInMenu()
    {
        if (_isLogInMenu)
        {
            if (GUI.Button(new Rect(10, Screen.height / 2 - 100, 150, 25), "New Game"))
            {
                // with this we load a level; that is we load the a new scene
                Application.LoadLevel("_Complete_Casino"); // the name with which we call this function needs to be the same as the scene name that we are loading
            }
            if (GUI.Button(new Rect(10, Screen.height / 2 - 65, 150, 25), "Load Game"))
            {
                _isLogInMenu = false;
                _isLoadGameMenu = true;
            }
            if (GUI.Button(new Rect(10, Screen.height / 2 - 30, 150, 25), "Level Select"))
            {
                _isLogInMenu = false;
                _isLevelSelecMenu = true;
            }
            if (GUI.Button(new Rect(10, Screen.height / 2 + 5, 150, 25), "Options"))
            {
                _isLogInMenu = false;
                _isOptionsMenu = true;
            }
            if (GUI.Button(new Rect(10, Screen.height / 2 + 40, 150, 25), "Log Out"))
            {
                //log out from the server
                _isLogInMenu = false;
                _isFirstMenu = true; // for now i just go back dirrectly to the first menus
            }
        }
    }
}
