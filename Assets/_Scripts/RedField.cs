﻿/**************************************************************
 * Author: Miroslav Arsov                                     *
 * date:   October, 2015                                      *
 *                                                            *
 * Description: The RedField class is attached to every field *
 * object that the players can put chips on. The script has a *
 * reference of the main camera and with the method of        *
 * casting rays it finds out the field where the mouse is     *
 * hovering and highlights the appropriate field or group of  *
 * fields.                                                    *
 **************************************************************/

using UnityEngine;

public class RedField : MonoBehaviour {

    private Color originalColor; // original field color on start-up
    private Renderer rend;       // renderer component of the field objects
    private FieldBets fldBets;   // reference to the BettingTracker component

    // array of object field names
    private string[] fldNames = { "Zero",
                                  "One", "Two", "Three", "Four",
                                  "Five", "Six", "Seven", "Eight",
                                  "Nine", "Ten", "Eleven", "Twelve",
                                  "Thirteen", "Fourteen", "Fifteen", "Sixteen",
                                  "Seventeen", "Eighteen", "Nineteen", "Twenty",
                                  "TwentyOne", "TwentyTwo", "TwentyThree", "TwentyFor",
                                  "TwentyFive", "TwentySix", "TwentySeven", "TwentyEight",
                                  "TwentyNine", "Thirty", "ThirtyOne", "ThirtyTwo",
                                  "ThirtyThree", "ThirtyFour", "ThirtyFive", "ThirtySix"};

    // lookup tables for all the different outside bets that are contained of multiple numbers
    private int[] firstDozen = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    private int[] secondDozen = {13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};
    private int[] thirdDozen = { 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36};
    private int[] red = {1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36};
    private int[] black = {2, 4, 6, 8, 10, 11, 13,15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35};
    private int[] oneToHalf = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
    private int[] halfToEnd = {19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36};
    private int[] even = {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36};
    private int[] odd = {1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35};
    private int[] twoToOne_1 = { 1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34 };
    private int[] twoToOne_2 = { 2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35 };
    private int[] twoToOne_3 = { 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36 };

    // initialization on loading of the objects
    void Start () {
        rend = GetComponent<Renderer>();

        // getting the reference the instance of the Field Bets object
        fldBets = GameObject.Find("BettingTracker").GetComponent<FieldBets> ();
    }
	
    // when the mosue enters the object; the objects highlights red
    void OnMouseEnter()
    {
        originalColor = rend.material.color;
        if (transform.tag == "sngl_fld")
        {
            rend.material.color = Color.red;
        }
        else
        {
            switch(transform.name)
            {
                case "FirstDozen":
                    highlightFields(true, firstDozen, Color.red);
                    break;
                case "SecondDozen":
                    highlightFields(true, secondDozen, Color.red);
                    break;
                case "ThirdDozen":
                    highlightFields(true, thirdDozen, Color.red);
                    break;
                case "Red":
                    highlightFields(true, red, Color.red);
                    break;
                case "Black":
                    highlightFields(true, black, Color.red);
                    break;
                case "OneToEighteen":
                    highlightFields(true, oneToHalf, Color.red);
                    break;
                case "EighteenToThirtySix":
                    highlightFields(true, halfToEnd, Color.red);
                    break;
                case "Even":
                    highlightFields(true, even, Color.red);
                    break;
                case "Odd":
                    highlightFields(true, odd, Color.red);
                    break;
                case "twoToOne_1":
                    highlightFields(true, twoToOne_1, Color.red);
                    break;
                case "twoToOne_2":
                    highlightFields(true, twoToOne_2, Color.red);
                    break;
                case "twoToOne_3":
                    highlightFields(true, twoToOne_3, Color.red);
                    break;
            }
        }
    }

    // restore the original color of the playing fields
    void OnMouseExit()
    {
        if (transform.tag == "sngl_fld")
        {
            rend.material.color = originalColor;
        }
        else
        {
            switch (transform.name)
            {
                case "FirstDozen":
                    highlightFields(false, firstDozen, Color.red);
                    break;
                case "SecondDozen":
                    highlightFields(false, secondDozen, Color.red);
                    break;
                case "ThirdDozen":
                    highlightFields(false, thirdDozen, Color.red);
                    break;
                case "Red":
                    highlightFields(false, red, Color.red);
                    break;
                case "Black":
                    highlightFields(false, black, Color.red);
                    break;
                case "OneToEighteen":
                    highlightFields(false, oneToHalf, Color.red);
                    break;
                case "EighteenToThirtySix":
                    highlightFields(false, halfToEnd, Color.red);
                    break;
                case "Even":
                    highlightFields(false, even, Color.red);
                    break;
                case "Odd":
                    highlightFields(false, odd, Color.red);
                    break;
                case "twoToOne_1":
                    highlightFields(false, twoToOne_1, Color.red);
                    break;
                case "twoToOne_2":
                    highlightFields(false, twoToOne_2, Color.red);
                    break;
                case "twoToOne_3":
                    highlightFields(false, twoToOne_3, Color.red);
                    break;
            }
        }
    }

    // when we click inside the fields with chip selected we message the FieldBets object to create a bet
    void OnMouseDown()
    {
        int chipSelected = 0;
        chipSelected = GameObject.Find("Main Camera").GetComponent<MainCamera>().chipValue;
        
        // check if a chip is selected and if it is, for every click add a bet for amount equal to the chip bet
        if (chipSelected != 0 && fldBets.canCreatBet(chipSelected))
        {
            fldBets.CreateBet(chipSelected, transform.name);
        }
    }

    // execute highlighting of the array of fields specified in the arrayOfFields argument 
    void highlightFields(bool highlightIt, int[] arrayOfFields, Color highlightColor)
    {
        GameObject currentObj;
 
        for(int i=0; i< arrayOfFields.Length; i++)
        {
            currentObj = GameObject.Find(fldNames[arrayOfFields[i]]);

            if(highlightIt)
            {
                //highlight it with the color that is specified in the input argument
                currentObj.GetComponent<Renderer>().material.color = highlightColor;
            }
            else
            {
                //restore original color
                currentObj.GetComponent<Renderer>().material.color = originalColor;
            }
        }
    }
}
