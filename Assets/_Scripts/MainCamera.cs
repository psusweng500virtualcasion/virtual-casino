﻿/**************************************************************
 * Author: Miroslav Arsov                                     *
 * date:   October, 2015                                      *
 *                                                            *
 * Description: The MainCamera class is attached to the main  *
 * camera object that is representing the game view while the *
 * game is being played. The script itself is tasked          *
 * ofidentifying the stack of chips object on which the mouse *
 * is hovering and on mouse click it replaces the mouse       *
 * pointer with a chip of the same value.                     *
 **************************************************************/

using UnityEngine;
using UnityEngine.UI;

public class MainCamera : MonoBehaviour {

	public string chipValueStr;  //text that showes up to display the chips amount when we mouse over it
    public int chipValue;      // chip value published for other object to use
    
    // The texture that's going to replace the current cursor
	// for every different chip value that we are going over
	public Texture2D cursorTexture_1dolar;
	public Texture2D cursorTexture_5dolar;
	public Texture2D cursorTexture_10dolar;
	public Texture2D cursorTexture_20dolar;
	public Texture2D cursorTexture_50dolar;
	public Texture2D cursorTexture_100dolar;
	
	public bool ccEnabled = false; // This variable flags whether the custom cursor is active or not 
    private RaycastHit rayHit;     // ray object thrown from the camera towards the mouse
	private GameObject colliderObj;// colider object
	private Camera cmra;           // reference to the main camera

    // initialization on loading of the objects
    void Start () {
		//initialize text and disable it so it's not visible
		chipValueStr = "";
		//chipValueStr.gameObject.SetActive (false);

        chipValue = 0; // initialize the value of the current chip selection

        //get the camera object
        cmra = GetComponent<Camera> ();

	}

    // when disabled reset the cursor to it's original settings
    void OnDisable()  
	{ 
		//Resets the cursor to the default 
		Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto); 
		//Set the _ccEnabled variable to false 
		this.ccEnabled = false; 
	} 
	
	// Update is called once per frame
	void Update () {
	
		// every frame cast a ray from the camera to the mouse pointer direction
		// and find the object that the mouse is pointing towards
		Ray ray = cmra.ScreenPointToRay (Input.mousePosition);
		if (Input.GetMouseButton(0) && Physics.Raycast (ray.origin, ray.direction, out rayHit)) {
			colliderObj = rayHit.collider.gameObject;
			chipValueStr = colliderObj.name;
			//chipValueStr.gameObject.SetActive(true);

			//load custom cursor depending on the what chip you click on
			switch( chipValueStr )
				{
					//call the cursor to the correct texture
				case "Stack_1dolar":
					SetCustomCursor( this.cursorTexture_1dolar );
                    chipValue = 1;
					break;
				case "Stack_5dolar":
					SetCustomCursor( this.cursorTexture_5dolar );
                    chipValue = 5;
                    break;
				case "Stack_10dolar":
					SetCustomCursor( this.cursorTexture_10dolar );
                    chipValue = 10;
                    break;
				case "Stack_20dolar":
					SetCustomCursor( this.cursorTexture_20dolar );
                    chipValue = 20;
                    break;
				case "Stack_50dolar":
					SetCustomCursor( this.cursorTexture_50dolar );
                    chipValue = 50;
                    break;
				case "Stack_100dolar":
					SetCustomCursor( this.cursorTexture_100dolar );
                    chipValue = 100;
                    break;

				default:
					SetCustomCursor(null);
				break;
				}
		
		}
	}

	private void SetCustomCursor(Texture2D cursTexture) 
	{ 
		//Replace the 'cursorTexture' with the cursor   
		Cursor.SetCursor(cursTexture, Vector2.zero, CursorMode.Auto); 

		//Set the ccEnabled variable to true 
		this.ccEnabled = true; 
	}
}
