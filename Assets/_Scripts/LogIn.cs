﻿using UnityEngine;
using System.Collections;
using System;
using Mono.Data.Sqlite;
using System.Data; 


public class LogIn : MonoBehaviour {
#region Variables
    //static variables
    public static string Email = "";
    public static string Password = "";
    public static string Status = "";
    
    //public variables
    public string CurrentMenu = "Login";

    //private variables
    //for confirm email and password
    private string ConfirmPass = "";
    private string ConfirmEmail = "";
    //for create email and password so they are not mixed with logging email and password
    private string CEmail = "";
    private string CPassword = "";
    
    //GUI Test section
    public float X;
    public float Y;
    public float Width;
    public float Height;
    
#endregion

    // Use this for initialization
    void Start () {

    } //End Start method 

    private bool CheckUser(string userEmail, string userPassword)
    {
        bool isAvailable = false;
        string conn = "URI=file:" + Application.dataPath + "/Player.s3db"; //Path to database.
        IDbConnection dbconn;
        dbconn = (IDbConnection)new SqliteConnection(conn);
        dbconn.Open(); //Open connection to the database.

        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = "SELECT *" + "FROM Account WHERE Email= '" + userEmail + "'";
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            string email = reader.GetString(0);
            string pass = reader.GetString(1);

            if(email == userEmail && pass == userPassword)
            {
                isAvailable = true;
            }
            Debug.Log("email= " + email + "  pass =" + pass);
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;

        return isAvailable;
    }

    void InsertUser()
    {
        string conn = "URI=file:" + Application.dataPath + "/Player.s3db"; //Path to database.
        IDbConnection dbconn;
        dbconn = (IDbConnection)new SqliteConnection(conn);
        dbconn.Open(); //Open connection to the database.

        CPassword = GUI.TextField(new Rect(320, 370, 220, 25), CPassword);
        CEmail = GUI.TextField(new Rect(320, 320, 220, 25), CEmail);

        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = "insert into Account (Email, Password) values('" + CEmail + "', '" + CPassword + "')";
        dbcmd.CommandText = sqlQuery;

        dbcmd.ExecuteNonQuery();
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;

    }


    //Main GUI function;
    void OnGUI ()
    {
        //If current menu = login call then call the login menu
        //by calling our logingui function. Else display the
        //create account gui by calling its function
        if (CurrentMenu == "Login"){
            LoginGui();

            } else if (CurrentMenu == "CreateAccount") {
            CreateAccountGUI();
        }

    } //End OnGui

#region Custom methods

    //This method will login the accounts
    void LoginGui(){
        //Create box to stimulate window 
        GUI.Box(new Rect(240, 200, (Screen .width/4) + 200, (Screen.height/4) + 250), "Login");

        //create account and login button
        //open create account window
        if (GUI .Button (new Rect (300, 410, 120 , 25), "Create Account")){
            CurrentMenu = "CreateAccount";
           
        }

        //Create Email label and text box
        GUI.Label(new Rect(320, 300, 220, 25), "Email:");
        Email = GUI.TextField(new Rect(320, 320, 220, 25), Email);

        //Create password label and text box
        GUI.Label(new Rect(320, 350, 220, 25), "Password:");
        Password = GUI.TextField(new Rect(320, 370, 220, 25), Password);

        //create Status
        GUI.Label(new Rect(320, 575, 220, 25), "Status:");
        Status = GUI.TextField(new Rect(380, 575, 220, 25), Status);

        if (GUI.Button(new Rect(440, 410, 120, 25), "Log In"))
        {
            bool loginSuccessful = CheckUser(Email, Password);

            if (loginSuccessful)
            {
                //user exists in the database
                Application.LoadLevel("_Complete_Casino");
            }
            else
            {
                //user doesnot exit in the database
                Debug.Log("Unsuccessful");
                Status = "Invald Username / Password";
                //GUI.Label(new Rect(540, 410, 120, 45), "No User Found");

            }

           
        }
        
            
    } //End LoginGui


    void CreateAccountGUI()
    {

        //Create box to stimulate window 
        GUI.Box(new Rect(240, 200, (Screen.width / 4) + 200, (Screen.height / 4) + 250), "Create Account");

        //Create Email label and text box
        GUI.Label(new Rect(320, 300, 220, 25), "Email:");
        CEmail = GUI.TextField(new Rect(320, 320, 220, 25), CEmail);

        //Create password label and text box
        GUI.Label(new Rect(320, 350, 220, 25), "Password:");
        CPassword = GUI.TextField(new Rect(320, 370, 220, 25), CPassword);

        //Create confirm email label and text box
        GUI.Label(new Rect(320, 400, 220, 25), "Confirm Email:");
        ConfirmEmail = GUI.TextField(new Rect(320, 420, 220, 25), ConfirmEmail);

        //Create confirm password label and text box 
        GUI.Label(new Rect(320, 450, 220, 25), "Confirm Password:");
        ConfirmPass = GUI.TextField(new Rect(320, 470, 220, 25), ConfirmPass);

        //create Status
        GUI.Label(new Rect(320, 575, 220, 25), "Status:");
        Status = GUI.TextField(new Rect(380, 575, 240, 25), Status);

        //create account and back button
        //open create account window
        if (GUI.Button(new Rect(300, 510, 120, 25), "Create Account"))
        {
            if (ConfirmPass == CPassword && ConfirmEmail == CEmail) { 
               InsertUser ();
            Debug.Log("User Account Created Successfully");
                Status = "User Account Created Successfully";
              
            }
            else if (ConfirmPass != CPassword && ConfirmEmail != CEmail)
            {
                Debug.Log("Emails and Passwords do not match");
                Status = "Emails and Passwords do not match";
            }
            else if (ConfirmEmail != CEmail)
            {
                Debug.Log("Emails do not match");
                Status = "Emails do not match";
            }
            else
            {
                Debug.Log("Passwords do not match");
                Status = "Passwords do not match";
               
            }



        }//end create account buutton

        if (GUI.Button(new Rect(440, 510, 120, 25), "Back"))
        {
            CurrentMenu = "Login";
        }//end buttons 

    } //End CreateAccountGUI

    #endregion

    

} //End Class 

