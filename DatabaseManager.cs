﻿using UnityEngine;
using System.Data;
using Mono.Data.SqliteClient;
using System.IO;

namespace Dao
{
    class DatabaseManager : MonoBehaviour
    {
        public static DatabaseManager Instance = null;
        public bool DebugMode = false;

        /// <summary>
        /// Table name and DB actual file location
        /// </summary>
        private const string SQL_DB_NAME = "VirtualCasinoDB";

        // feel free to change where the DBs are stored
        // this file will show up in the Unity inspector after a few seconds of running it the first time
        private static readonly string SQL_DB_LOCATION = "URI=file:"
            + Application.dataPath + Path.DirectorySeparatorChar
            + "Plugins" + Path.DirectorySeparatorChar
            + "SQLiter" + Path.DirectorySeparatorChar
            + "Databases" + Path.DirectorySeparatorChar
            + SQL_DB_NAME + ".db";

        /// <summary>
        /// DB objects
        /// </summary>
        protected IDbConnection mConnection = null;
        protected IDbCommand mCommand = null;
        protected IDataReader mReader = null;
        protected string mSQLString;

        private bool mCreateNewTable = false;

        /// <summary>
        /// Awake will initialize the connection.  
        /// RunAsyncInit is just for show.  You can do the normal SQLiteInit to ensure that it is
        /// initialized during the AWake() phase and everything is ready during the Start() phase
        /// </summary>
        void Awake()
        {
            Debug.Log(SQL_DB_LOCATION);
            Instance = this;
            DatabaseInit();
        }

        void Start()
        {
            // just for testing, uncomment to play with it
            Invoke("Test", 1);
        }

        /// <summary>
        /// Clean up SQLite Connections, anything else
        /// </summary>
        void OnDestroy()
        {
            DatabaseClose();
        }

        /// <summary>
        /// Example using the Loom to run an asynchronous method on another thread so SQLite lookups
        /// do not block the main Unity thread
        /// </summary>
        public void RunAsyncInit()
        {
            SQLiter.LoomManager.Loom.QueueOnMainThread(() =>
            {
                DatabaseInit();
            });
        }

        /// <summary>
        /// Basic initialization of SQLite
        /// </summary>
        private void DatabaseInit()
        {
            /*Debug.Log("SQLiter - Opening SQLite Connection");
            mConnection = new SqliteConnection(SQL_DB_LOCATION);
            mCommand = mConnection.CreateCommand();
            mConnection.Open();*/
            DatabaseConnect();

            // WAL = write ahead logging, very huge speed increase
            mCommand.CommandText = "PRAGMA journal_mode = WAL;";
            mCommand.ExecuteNonQuery();

            // journal mode = look it up on google, I don't remember
            mCommand.CommandText = "PRAGMA journal_mode";
            mReader = mCommand.ExecuteReader();
            if (DebugMode && mReader.Read())
                Debug.Log("SQLiter - WAL value is: " + mReader.GetString(0));
            mReader.Close();

            // more speed increases
            mCommand.CommandText = "PRAGMA synchronous = OFF";
            mCommand.ExecuteNonQuery();

            // and some more
            mCommand.CommandText = "PRAGMA synchronous";
            mReader = mCommand.ExecuteReader();
            if (DebugMode && mReader.Read())
                Debug.Log("SQLiter - synchronous value is: " + mReader.GetInt32(0));
            mReader.Close();

            // here we check if the table you want to use exists or not.  If it doesn't exist we create it.
            /*mCommand.CommandText = "SELECT name FROM sqlite_master WHERE name='" + SQL_TABLE_NAME + "'";
            mReader = mCommand.ExecuteReader();
            if (!mReader.Read())
            {
                Debug.Log("SQLiter - Could not find SQLite table " + SQL_TABLE_NAME);
                mCreateNewTable = true;
            }
            mReader.Close();*/

            // create new table if it wasn't found
            /*if (mCreateNewTable)
            {
                Debug.Log("SQLiter - Creating new SQLite table " + SQL_TABLE_NAME);

                // insurance policy, drop table
                mCommand.CommandText = "DROP TABLE IF EXISTS " + SQL_TABLE_NAME;
                mCommand.ExecuteNonQuery();

                // create new - SQLite recommendation is to drop table, not clear it
                mSQLString = "CREATE TABLE IF NOT EXISTS " + SQL_TABLE_NAME + " (" +
                    COL_NAME + " TEXT UNIQUE, " +
                    COL_RACE + " INTEGER, " +
                    COL_CLASS + " INTEGER, " +
                    COL_GOLD + " INTEGER, " +
                    COL_LOGIN_LAST + " INTEGER, " +
                    COL_LEVEL + " INTEGER, " +
                    COL_XP + " INTEGER)";
                mCommand.CommandText = mSQLString;
                mCommand.ExecuteNonQuery();
            }
            else
            {
                if (DebugMode)
                    Debug.Log("SQLiter - SQLite table " + SQL_TABLE_NAME + " was found");
            }*/

            // close connection
            mConnection.Close();
        }

        /// <summary>
        /// Basic execute command - open, create command, execute, close
        /// </summary>
        /// <param name="commandText"></param>
        public void ExecuteNonQuery(string commandText)
        {
            mConnection.Open();
            mCommand.CommandText = commandText;
            mCommand.ExecuteNonQuery();
            mConnection.Close();
        }

        /// <summary>
        /// Establishes the database connection
        /// </summary>
        protected void DatabaseConnect()
        {
            Debug.Log("SQLiter - Opening SQLite Connection");
            mConnection = new SqliteConnection(SQL_DB_LOCATION);
            mCommand = mConnection.CreateCommand();
            mConnection.Open();
        }

        /// <summary>
        /// Determines if a table has been created
        /// </summary>
        protected bool doesTableExists(string tableName)
        {
            mCommand.CommandText = "SELECT name FROM sqlite_master WHERE name='" + tableName + "'";
            mReader = mCommand.ExecuteReader();
            if (!mReader.Read())
            {
                Debug.Log("SQLiter - Could not find SQLite table " + tableName);
                mReader.Close();
                return false;
            }
            else
            {
                mReader.Close();
                return true;
            }
        }

        /// <summary>
        /// Clean up everything for SQLite
        /// </summary>
        protected void DatabaseClose()
        {
            if (mReader != null && !mReader.IsClosed)
                mReader.Close();
            mReader = null;

            if (mCommand != null)
                mCommand.Dispose();
            mCommand = null;

            if (mConnection != null && mConnection.State != ConnectionState.Closed)
                mConnection.Close();
            mConnection = null;
        }
    }
}
