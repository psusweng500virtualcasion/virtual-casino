﻿using UnityEngine;
using System.Data;
using Mono.Data.SqliteClient;
using System.IO;

namespace Dao
{
    class PlayerDao : DatabaseManager
    {
        // table name
        private const string SQL_TABLE_NAME = "Players";

        /// <summary>
        /// predefine columns here to there are no typos
        /// </summary>
        private const string COL_ID = "id";  // primary unique key
        private const string COL_USER_NAME = "username";
        private const string COL_PASSWORD = "password";
        private const string COL_FIRST_NAME = "firstName";
        private const string COL_LAST_NAME = "lastName";
        private const string COL_BANK = "bank";

        public PlayerDao() {
            DatabaseConnect();

            if (! doesTableExists(SQL_TABLE_NAME)) {
                createTable();
            }
        }

        // Creates table if it doesn't exist
        private void createTable() {
            Debug.Log("SQLiter - Creating new SQLite table " + SQL_TABLE_NAME);

            // insurance policy, drop table
            mCommand.CommandText = "DROP TABLE IF EXISTS " + SQL_TABLE_NAME;
            mCommand.ExecuteNonQuery();

            // create new - SQLite recommendation is to drop table, not clear it
            mSQLString = "CREATE TABLE IF NOT EXISTS " + SQL_TABLE_NAME + " (" +
                COL_ID + " TEXT UNIQUE, " +
                COL_USER_NAME + " TEXT, " +
                COL_PASSWORD + " TEXT, " +
                COL_FIRST_NAME + " TEXT, " +
                COL_LAST_NAME + " TEXT, " +
                COL_BANK + " INTEGER)";
            mCommand.CommandText = mSQLString;
            mCommand.ExecuteNonQuery();

            DatabaseClose();
        }

        // Inserts a new Player into the database
        // TODO: Add the player object
        public void InsertPlayer(object player) {
            mSQLString = "INSERT OR REPLACE INTO " + SQL_TABLE_NAME
                + " ("
                + COL_ID + ","
                + COL_USER_NAME + ","
                + COL_PASSWORD + ","
                + COL_FIRST_NAME + ","
                + COL_LAST_NAME + ","
                + COL_BANK
                + ") VALUES ('"
                + player.id + "','"  // note that string values need quote or double-quote delimiters
                + player.userName + "','"
                + player.password + "',"
                + player.firstName + "','"
                + player.lastName + "',"
                + player.bank
                + ");";

            if (DebugMode)
                Debug.Log(mSQLString);
            ExecuteNonQuery(mSQLString);
        }

        // Updates a particular player into the database
        // TODO: Add the player object
        public void UpdatePlayer(object player) {
            ExecuteNonQuery(
                "UPDATE OR REPLACE " + SQL_TABLE_NAME + " SET " + COL_USER_NAME + "= '" + player.userName + 
                "', " + COL_PASSWORD + "= '" + player.password +
                "', " + COL_FIRST_NAME + "= '" + player.firstName +
                "', " + COL_LAST_NAME + "= '" + player.lastName +
                "', " + COL_BANK + "=" + player.bank +
                " WHERE " + COL_ID + "='" + player.id + "'");
        }

        // Deletes a particular player from the database
        // TODO: Add the player object
        public void DeletePlayer(object player) {
            ExecuteNonQuery("DELETE FROM " + SQL_TABLE_NAME + " WHERE " + COL_ID + "='" + player.id + "'");
        }

        // Select a particular player from the database
        // TODO: Add the player object
        public object FindPlayer(string playerId) {
            mConnection.Open();

            // if you have a bunch of stuff, this is going to be inefficient and a pain.  it's just for testing/show
            mCommand.CommandText = "SELECT " + 
                COL_ID + "," + 
                COL_USER_NAME + "," +
                COL_PASSWORD + "," +
                COL_FIRST_NAME + "," +
                COL_LAST_NAME + "," +
                COL_BANK + " FROM " + SQL_TABLE_NAME + 
                " WHERE id = '" + playerId + "'";
            mReader = mCommand.ExecuteReader();
            object player = new Object();

            while (mReader.Read())
            {
                player.id = mReader.GetString(0);
                player.userName = mReader.GetString(1);
                player.password = mReader.GetString(2);
                player.firstName = mReader.GetString(3);
                player.lastName = mReader.GetString(4);
                player.bank = mReader.GetInt32(5);

                // view our output
                if (DebugMode)
                    Debug.Log(sb.ToString());
            }
            mReader.Close();
            mConnection.Close();
        }
    }
}